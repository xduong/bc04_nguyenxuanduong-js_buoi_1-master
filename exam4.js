/*
 *
 * input
 * chiều dài và chiều rộng của hình chữ nhật
 *
 *
 *
 *
 *
 * todo
 * tạo biến chiều dài và chiều rộng (a, b) và biến kết quả tính chu vi và diện tích (P, S)
 * tính chu vi hình chữ nhật: (dài + rộng)*2
 * P = (a+b)*2
 * tính diện tích hình chữ nhật: dài * rộng
 * S = a*b
 *
 *
 *
 *
 *
 *
 * output:
 * chu vi hình chữ nhật
 * diện tích hình chữ nhật
 */

var a, b, P, S;
a = 4;
b = 5;
P = (a + b) * 2;
console.log("chu vi: ", P);
S = a * b;
console.log("dien tich: ", S);
